package no.ntnu.idatg2001.mappe.del1.hospital;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class HospitalTest {

    @Test
    @DisplayName("Hospital constructor test negative null value")
    public void hospitalConstructorTestNegativeNull()  {
        try  {
            Hospital testHospital = new Hospital(null);
            fail("Should throw Null Pointer Exception");
        }
        catch (NullPointerException e)  {
            assert(true);
        }
    }

    @Test
    @DisplayName("Hospital constructor test negative blank value")
    public void hospitalConstructorTestNegativeBlank()  {
        try  {
            Hospital testHospital = new Hospital("");
            fail("Should throw Illegal Argument Exception");
        }
        catch (IllegalArgumentException e)  {
            assert(true);
        }
    }

    @Test
    @DisplayName("Hospital constructor test positive")
    public void hospitalConstructorTestPositive()  {
        Hospital testHospital = new Hospital("Test Hospital");
        assertEquals("Test Hospital", testHospital.getHospitalName());
        assertEquals(HashMap.class,testHospital.getDepartments().getClass());
    }

    @Test
    @DisplayName("Get hospital positive test")
    public void getHospitalTestPositive()  {
        Hospital testHospital = new Hospital("Test Hospital");
        assertEquals("Test Hospital", testHospital.getHospitalName());
    }

    @Test
    @DisplayName("Get departments positive test")
    public void getDepartmentsTestPositive()  {
        Hospital testHospital = new Hospital("Test Hospital");
        assertEquals(HashMap.class, testHospital.getDepartments().getClass());
    }

    @Test
    @DisplayName("Add department test positive")
    public void addDepartmentTestPositive()  {
        Hospital testHospital = new Hospital("Test Hospital");
        testHospital.addDepartment(new Department("Test Department"));
        assertEquals(1, testHospital.getDepartments().size());
        assertEquals("Test Department", testHospital.getDepartments().get("Test Department").getDepartmentName());
    }

    @Test
    @DisplayName("Add department test negative, null value")
    public void addDepartmentTestNegativeNull()  {
        Hospital testHospital = new Hospital("Test Hospital");
        try  {
            testHospital.addDepartment(null);
            fail("Test should throw Null Pointer Exception");
        }
        catch (NullPointerException e)  {
            assert (true);
        }
    }

    @Test
    @DisplayName("Add department test negative, department already registered")
    public void addDepartmentTestNegativeMapContainsKey()  {
        Hospital testHospital = new Hospital("Test Hospital");
        testHospital.addDepartment(new Department("Test Department"));
        try  {
            testHospital.addDepartment(new Department("Test Department"));
            fail("Test should throw Illegal Argument Exception");
        }
        catch (IllegalArgumentException e)  {
            assert (true);
        }
    }
}