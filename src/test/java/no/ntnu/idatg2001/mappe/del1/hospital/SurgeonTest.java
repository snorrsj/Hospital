package no.ntnu.idatg2001.mappe.del1.hospital;

import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SurgeonTest {

    @Test
    @DisplayName("Set diagnosis positive test")
    public void setDiagnosisTestPositive()  {
        Surgeon surgeon = new Surgeon("FirstSurgeon", "LastSurgeon", "987");
        Patient patient = new Patient("FirstPatient", "LastPatient", "123");
        surgeon.setDiagnosis(patient, "Fever");
        assertEquals("Fever", patient.getDiagnosis());
    }

    @Test
    @DisplayName("Set diagnosis negative test, diagnosis set to null")
    public void setDiagnosisTestNegative()  {
        Surgeon surgeon = new Surgeon("FirstSurgeon", "LastSurgeon", "987");
        Patient patient = new Patient("FirstPatient", "LastPatient", "123");
        try  {
            surgeon.setDiagnosis(patient, null);
            fail("Test should throw Null Pointer Exception");
        }
        catch (NullPointerException e)  {
            assert (true);
        }
    }
}