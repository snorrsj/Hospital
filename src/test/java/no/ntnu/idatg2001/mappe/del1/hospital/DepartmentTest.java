package no.ntnu.idatg2001.mappe.del1.hospital;

import no.ntnu.idatg2001.mappe.del1.hospital.exception.RemoveException;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Employee;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Nurse;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    private static Department departmentTestStatic;

    @BeforeAll
    public static void setUp()  {
        departmentTestStatic = new Department("departmentTestStatic");
    }

    @Test
    @DisplayName("Constructor test positive")
    public void departmentConstructorTestPositive()  {
        Department testDepartment = new Department("Test Department");
        assertEquals("Test Department", testDepartment.getDepartmentName());
        assertEquals(HashMap.class, testDepartment.getEmployees().getClass());
        assertEquals(HashMap.class, testDepartment.getPatients().getClass());
    }

    @Test
    @DisplayName("Constructor test negative, null parameter")
    public void departmentConstructorTestNegativeNull()  {
        try  {
            Department testDepartment = new Department(null);
            fail("Test should throw Null Pointer Exception");
        }
        catch (NullPointerException d)  {
            assert(true);
        }
    }

    @Test
    @DisplayName("Remove person test negative, no match found employee")
    public void removeTestNegativeEmployee()  {
        Employee testPerson = new Employee("First", "Last", "987");
        Department testDepartment = new Department("Department");
        try  {
            testDepartment.remove(testPerson);
            fail("Test should throw Remove Exception");
        }
        catch (RemoveException e)  {
            assert(true);
        }
    }

    @Test
    @DisplayName("Remove person test negative, no match found patient")
    public void removeTestNegativePatient()  {
        Patient testPerson = new Patient("First", "Last", "987");
        Department testDepartment = new Department("Department");
        try  {
            testDepartment.remove(testPerson);
            fail("Test should throw Remove Exception");
        }
        catch (RemoveException e)  {
            assert(true);
        }
    }

    @Test
    @DisplayName("Remove person test positive employee")
    public void removePersonTestPositiveEmployee() throws RemoveException  {
        Surgeon testEmployee = new Surgeon("First", "Last", "987");
        Department testDepartment = new Department("Department");
        testDepartment.addEmployee(testEmployee);
        assertEquals(1,testDepartment.getEmployees().size());
        testDepartment.remove(testEmployee);
        assertEquals(0, testDepartment.getEmployees().size());
    }

    @Test
    @DisplayName("Remove person test positive patient")
    public void removePersonTestPositivePatient() throws RemoveException  {
        Patient testPatient = new Patient("First", "Last", "987");
        Department testDepartment = new Department("Department");
        testDepartment.addPatient(testPatient);
        assertEquals(1,testDepartment.getPatients().size());
        testDepartment.remove(testPatient);
        assertEquals(0, testDepartment.getPatients().size());
    }

    @Test
    @DisplayName("Add employee test positive, new person")
    public void addEmployeeTestPositiveNewPerson()  {
        Department testDepartment = new Department("Test");
        testDepartment.addEmployee(new Surgeon("First", "Last", "123"));
        assertEquals("First Last",testDepartment.getEmployees().get("123").getFullName());
    }

    @Test
    @DisplayName("Add employee test negative, employee with matching social security number in register")
    public void addEmployeeTestNegative()  {
        Department testDepartment = new Department("Test");
        Nurse testNurse = new Nurse("FirstNurse", "LastNurse", "123");
        testDepartment.addEmployee(testNurse);
        assertEquals(1, testDepartment.getEmployees().size());
        Employee testEmployee = new Employee("FirstEmployee", "LastEmployee", "123");
        try {
            testDepartment.addEmployee(testEmployee);
            fail("Should throw illegal argument exception");
        }
        catch  (IllegalArgumentException e){
            assert (true);
        }
    }

    @Test
    @DisplayName("Add patient test positive, new person")
    public void addPatientTestPositiveNewPerson()  {
        Department testDepartment = new Department("Test");
        testDepartment.addPatient(new Patient("First", "Last", "123"));
        assertEquals("First Last",testDepartment.getPatients().get("123").getFullName());
    }

    @Test
    @DisplayName("Add patient test negative, patient with matching social security number in register")
    public void addPatientTestNegative()  {
        Department testDepartment = new Department("Test");
        Patient testPatient = new Patient("FirstPatient", "LastPatient", "123");
        testDepartment.addPatient(testPatient);
        assertEquals(1, testDepartment.getPatients().size());
        Patient testPatient2 = new Patient("FirstName", "LastName", "123");
        try {
            testDepartment.addPatient(testPatient2);
            fail("Should throw illegal argument exception");
        }
        catch  (IllegalArgumentException e){
            assert (true);
        }
    }


    @Test
    @DisplayName("Get department name positive test")
    public void getDepartmentNameTest()  {
        assertEquals("departmentTestStatic", departmentTestStatic.getDepartmentName());
    }

    @Test
    @DisplayName("Set department name positive test")
    public void setDepartmentNameTest()  {
        Department testDepartment = new Department("Test");
        testDepartment.setDepartmentName("New name");
        assertEquals("New name", testDepartment.getDepartmentName());
    }

}