package no.ntnu.idatg2001.mappe.del1.hospital;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This test class tests all the Person methods as well as the methods belonging to Patient
 */
class PatientTest {

    private static Patient testPatientStatic;

    @BeforeAll
    public static void setUp()  {
        testPatientStatic = new Patient("FirstName", "LastName", "123");
    }


    @Test
    @DisplayName("Positive test for constructor")
    public void patientConstructorTestPositive()  {
        Patient testPatient = new Patient("FirstName", "LastName", "123");
        assertEquals("FirstName", testPatient.getFirstName());
        assertEquals("LastName", testPatient.getLastName());
        assertEquals("123", testPatient.getSocialSecurityNumber());
    }


    @Test
    @DisplayName("Negative test for constructor, null value")
    public void patientConstructorTestNegativeNull()  {
        try  {
            Person testPatient = new Patient("FirstName", null, "123");
            fail("Constructor should throw Null Pointer Exception");
        }
        catch (NullPointerException e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Negative test for constructor, blank value")
    public void patientConstructorTestNegativeBlank()  {
        try  {
            Person testPatient = new Patient("FirstName", "LastName", "");
            fail("Constructor should throw Illegal Argument Exception");
        }
        catch (IllegalArgumentException e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Positive test for get first name")
    public void getFirstNameTest()  {
        assertEquals("FirstName", testPatientStatic.getFirstName());
    }

    @Test
    @DisplayName("Positive test for set first name")
    public void setFirstNameTest()  {
        Person testPatient = new Patient("FirstName", "LastName", "123");
        testPatient.setFirstName("NewFirstName");
        assertEquals("NewFirstName", testPatient.getFirstName());
    }

    @Test
    @DisplayName("Positive test for get last name")
    public void getLastNameTest()  {
        assertEquals("LastName", testPatientStatic.getLastName());
    }


    @Test
    @DisplayName("Positive test for set last name")
    public void setLastNameTest()  {
        Person testPatient = new Patient("FirstName", "LastName", "123");
        testPatient.setLastName("NewLastName");
        assertEquals("NewLastName", testPatient.getLastName());
    }

    @Test
    @DisplayName("Positive test for get social security number")
    public void getSocialSecurityNumberTest()  {
        assertEquals("123", testPatientStatic.getSocialSecurityNumber());
    }

    @Test
    @DisplayName("Positive test for set social security number")
    public void setSocialSecurityNumberTest()  {
        Person testPatient = new Patient("FirstName", "LastName", "123");
        testPatient.setSocialSecurityNumber("12345");
        assertEquals("12345", testPatient.getSocialSecurityNumber());
    }

    @Test
    @DisplayName("Positive test for get full name")
    public void getFullNameTest()  {
        assertEquals("FirstName LastName",testPatientStatic.getFullName());
    }

    @Test
    @DisplayName("Positive test for get diagnosis")
    public void getDiagnosisTestPositive()  {
        assertEquals("",testPatientStatic.getDiagnosis());
    }

    @Test
    @DisplayName("Positive test for set diagnosis")
    public void setDiagnosisTestPositive()  {
        Patient testPatient = new Patient("FirstName", "LastName", "123");
        testPatient.setDiagnosis("Test Diagnosis");
        assertEquals("Test Diagnosis", testPatient.getDiagnosis());
    }

    @Test
    @DisplayName("Negative test for set diagnosis, null input")
    public void setDiagnosisTestNegative()  {
        Patient testPatient = new Patient("FirstName", "LastName", "123");
        try  {
            testPatient.setDiagnosis(null);
            fail("Diagnosis set to null should throw Null Pointer Exception");
        }
        catch (NullPointerException e) {
        }
    }


















}