package no.ntnu.idatg2001.mappe.del1.hospital;

import java.util.HashMap;

/**
 * This class holds information about a hospital and its departments
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public class Hospital {

    // Fields
    private String hospitalName;
    // I use hash map for the register because using the name of the department as a key makes it easy to
    // access the departments from this class.
    private HashMap<String, Department> departments;

    /**
     * Constructor which takes 1 parameter
     *
     * @param hospitalName Name of the hospital
     */
    public Hospital(String hospitalName)    {
        // Guard condition
        if(hospitalName.isBlank())  {
            throw new IllegalArgumentException("Hospital name is blank");
        }
        else  {
            this.hospitalName = hospitalName;
            this.departments = new HashMap<>();
        }
    }

    /**
     * Getters an setters below
     */
    public String getHospitalName() {
        return this.hospitalName;
    }

    public HashMap<String, Department> getDepartments()  {
        return this.departments;
    }


    /**
     * This adds a department to the hospital. It takes 1 parameter
     *
     * @param department department to be added
     */
    public void addDepartment(Department department)  {
        if (department == null)  {
            throw new NullPointerException("Department trying to be added is null");
        }
        else if (departments.containsKey(department.getDepartmentName()))  {
            throw new IllegalArgumentException("Department named " + department.getDepartmentName() + " is already registered in register");
        }
        else  {
            this.departments.put(department.getDepartmentName(), department);
        }
    }

    @Override
    public String toString() {
        return "Hospital name: " + hospitalName + "\nNumber of departments: " + departments;
    }
}
