package no.ntnu.idatg2001.mappe.del1.hospital;

/**
 *  This abstract class contains the base information an functionality about a person to be registered in a hospital
 *  database.
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */
public abstract class Person {

    //Fields
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Constructor that takes 3 parameters.
     *
     * @param firstName first name
     * @param lastName last name
     * @param socialSecurityNumber social security number
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if (firstName.isBlank() || lastName.isBlank() || socialSecurityNumber.isBlank())  {
            throw new IllegalArgumentException("One or more of the parameters is blank");
        }
        else  {
            this.firstName = firstName;
            this.lastName = lastName;
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * Getters and setters
     */
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * This getter returns the full name, first name + last name.
     *
     * @return string of full name
     */
    public String getFullName()  {
        return this.firstName + " " + this.lastName;
    }

    @Override
    public String toString() {
        return "Name: " + this.getFullName() +
                "\nSocial Security Number: " + this.socialSecurityNumber;
    }
}
