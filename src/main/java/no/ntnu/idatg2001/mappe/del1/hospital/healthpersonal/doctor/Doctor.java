package no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.idatg2001.mappe.del1.hospital.Patient;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Employee;

/**
 * This is an abstract doctor class to be extended by specialized doctor classes
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public abstract class Doctor extends Employee {

    /**
     * Constructor for Doctor classes, it takes 3 parameters
     *
     * @param firstName First name
     * @param lastName Last name
     * @param socialSecurityNumber Social security number
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber)  {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This abstract method allows a doctor to set a diagnosis to a patient. It takes 2 parameters.
     *
     * @param patient the patient to be diagnosed
     * @param diagnosis String diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
