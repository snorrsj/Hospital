package no.ntnu.idatg2001.mappe.del1.hospital;

/**
 * This is a class for a patient at a hospital
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */
public class Patient extends Person implements Diagnosable {

    // Fields
    private String diagnosis = "";


    /**
     * Constructor for class, it takes 3 parameters
     *
     * @param firstName First name
     * @param lastName Last name
     * @param socialSecurityNumber Social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    // Getter
    protected String getDiagnosis() {
        return diagnosis;
    }


    /**
     * This method sets a new diagnosis to the patient. To be used by doctor classes with authorization to set diagnosis
     * on patients.
     *
     * @param diagnosis String diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        if (diagnosis == null)  {
            throw new NullPointerException("Diagnosis cannot be null");
        }
        else  {
            this.diagnosis = diagnosis;
        }
    }

    @Override
    public String toString() {
        return "Name: " + this.getFullName() +
                "\nSocial Security Number: " + this.getSocialSecurityNumber() +
                "\nPatient at hospital with diagnosis: " + this.diagnosis;
    }
}
