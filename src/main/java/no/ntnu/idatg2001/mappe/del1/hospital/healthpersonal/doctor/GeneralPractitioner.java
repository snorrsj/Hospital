package no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.idatg2001.mappe.del1.hospital.Patient;

/**
 * This is a class containing information about a general practitioner employed in a department at a hospital
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public class GeneralPractitioner extends Doctor {


    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }


    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return "Name: " + this.getFullName() +
                "\nSocial Security Number: " + this.getSocialSecurityNumber() +
                "\nEmployed at hospital with job: General Practitioner";
    }
}
