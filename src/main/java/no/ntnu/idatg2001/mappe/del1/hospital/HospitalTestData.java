package no.ntnu.idatg2001.mappe.del1.hospital;

import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Employee;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Nurse;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.doctor.Surgeon;

/**
 * This is test data to be used when running HospitalClient
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */
public class HospitalTestData {

    private HospitalTestData()  {
        // This should not be called
    }

    public static void fillRegisterWithTestData(final Hospital hospital) {

        // Add some departments
        Department emergency = new Department("Akutten");
        Department childrenPolyclinic = new Department("Barn Poliklinikk");

        // Add some employees and patients to the emergency department
        emergency.addEmployee(new Employee("Odd Even" , "Primtallet", "465645654"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "456654456"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "7898754"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "45621"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "8654246"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "87951233"));
        emergency.addPatient(new Patient("Inga", "Lykke", "87964"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "687678"));

        // Add some employees and patients to the children's polyclinic department
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "7223366"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "6666784"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "8886224"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "899999546"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "111213"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "22222365"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "666677777"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "888889641"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "1122166"));

        // Add the departments to the hospital
        hospital.addDepartment(emergency);
        hospital.addDepartment(childrenPolyclinic);
    }
}