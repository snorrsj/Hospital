package no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal;


/**
 * This is a class containing information about a nurse employed in a department at a hospital
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */
public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Name: " + this.getFullName() +
                "\nSocial Security Number: " + this.getSocialSecurityNumber() +
                "\nEmployed at hospital with job: Nurse";
    }
}
