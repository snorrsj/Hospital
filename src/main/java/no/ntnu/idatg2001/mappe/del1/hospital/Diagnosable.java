package no.ntnu.idatg2001.mappe.del1.hospital;

/**
 * This is an interface to be implemented by a patient in a hospital
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public interface Diagnosable {

    /**
     * This is a method for setting a diagnosis to a patient
     *
     * @param diagnosis String diagnosis
     */
    public void setDiagnosis(String diagnosis);
}
