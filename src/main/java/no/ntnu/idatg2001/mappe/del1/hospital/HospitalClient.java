package no.ntnu.idatg2001.mappe.del1.hospital;

import no.ntnu.idatg2001.mappe.del1.hospital.exception.RemoveException;


/**
 * This is a client used to run tests on the hospital system.
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public class HospitalClient {

    public static void main(String[] args) throws RemoveException {

        Hospital hospital = new Hospital("Test Hospital");

        HospitalTestData.fillRegisterWithTestData(hospital);

        // Variables created for easier to read and cleaner code. Person to be removed: Nina Teknologi // SSN: 8654246
        Department akutten = hospital.getDepartments().get("Akutten");
        Person ninaTeknologi = akutten.getEmployees().get("8654246");

        // View size of department before removal method is run
        System.out.println("\nDepartment before removal:\n");
        System.out.println(akutten.toString());

        // Run remove method and view size of department after removal method is run. Note that number of employees
        // changes from 6 to 5 - one employee has been removed.
        System.out.println("\n----\n\nDepartment after first removal, nurse removed:\n");
        akutten.remove(ninaTeknologi);
        System.out.println(akutten.toString());

        // Run remove method again, this time it throws an exception because the person to be removed is not found
        // in the register. Show the department info again inside the catch block to ensure that the catch has worked
        // correctly and that nothing has been removed from the department.
        try  {
            akutten.remove(ninaTeknologi);
        }
        catch (RemoveException e)  {
            System.out.println("\n----\n\nDepartment after second removal, exception thrown successsfully:\n");
            System.out.println(akutten.toString());
        }








    }

}
