package no.ntnu.idatg2001.mappe.del1.hospital.exception;

import java.io.Serial;
import java.io.Serializable;

/**
 * This is an exception class, it is to be thrown when a patient or an employee if an object is not found.
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */

public class RemoveException extends Exception implements Serializable {

    // Serialization number
    @Serial
    private final static long serialVersionUID = 1L;

    /**
     * Constructor used when throwing exception
     *
     * @param exceptionMessage Exception message
     */
    public RemoveException(String exceptionMessage)  {
        super(exceptionMessage);
    }
}
