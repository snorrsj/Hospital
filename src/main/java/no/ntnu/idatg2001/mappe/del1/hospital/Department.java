package no.ntnu.idatg2001.mappe.del1.hospital;

import no.ntnu.idatg2001.mappe.del1.hospital.exception.RemoveException;
import no.ntnu.idatg2001.mappe.del1.hospital.healthpersonal.Employee;

import java.util.HashMap;
import java.util.Objects;

/**
 * This is a class containing information about a hospital department. It has 2 registers - one for the employees in
 * this department and one for the patients.
 *
 * @author Snorre Sjaatil
 * @version 1.1.0
 */
public class Department {

    // Fields
    private String departmentName;
    // I use HashMap since I can map each person to their social security number and easily access them and check if
    // they are already in register. Every persons social security number is unique so this prevents duplicates.
    private HashMap<String, Person> employeeRegister;
    private HashMap<String, Person> patientRegister;

    /**
     * Constructor which takes 1 parameter
     *
     * @param departmentName Name of department to be created
     */
    public Department(String departmentName) {
        // Guard conditions to make sure object is created correctly
        if (departmentName.isBlank())  {
            throw new IllegalArgumentException("The name of department cannot be blank");
        }
        else  {
            this.departmentName = departmentName;
            this.employeeRegister = new HashMap<>();
            this.patientRegister = new HashMap<>();
        }
    }

    /**
     * Getters and setters below
     */
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public HashMap<String, Person> getEmployees() {
        return employeeRegister;
    }

    public HashMap<String, Person> getPatients() {
        return patientRegister;
    }




    /**
     * This adds an employee to the employee registry if the social security number of the employee is not already
     * in the register.
     *
     * @param employee the employee to be added to register
     */
    public void addEmployee(Employee employee)  {
        if (!employeeRegister.containsKey(employee.getSocialSecurityNumber()))  {
            this.employeeRegister.put(employee.getSocialSecurityNumber(), employee);
        }
        else  {
            throw new IllegalArgumentException("An employee with this social security number is already registered in the register.");

        }
    }

    /**
     * This adds a patient to the patient registry if the social security number of the employee is not already
     * in the register.
     *
     * @param patient the patient to be added to register
     */
    public void addPatient(Patient patient)  {
        if (!patientRegister.containsKey(patient.getSocialSecurityNumber()))  {
            this.patientRegister.put(patient.getSocialSecurityNumber(), patient);
        }
        else  {
            throw new IllegalArgumentException("A patient with this social security number is already registered in the register.");
        }
    }

    /**
     * This removes an employee or a patient from the corresponding registry.
     *
     * @throws RemoveException Throws remove exception when no person matching a social security number in department is found.
     * @param person employee or patient to be removed
     */
    public void remove(Person person) throws RemoveException
    {
        if (person instanceof Employee)
        {
            if (employeeRegister.containsKey(person.getSocialSecurityNumber()))  {
                employeeRegister.remove(person.getSocialSecurityNumber());
            }
            else  {
                throw new RemoveException("No employee matching social security number found in register");
            }
        }
        if (person instanceof Patient)
        {
            if (patientRegister.containsKey(person.getSocialSecurityNumber()))  {
                patientRegister.remove(person.getSocialSecurityNumber());
            }
            else  {
                throw new RemoveException("No patient matching social security number found in register");
            }
        }
    }


    @Override
    public String toString() {
        return "Department name: " + this.departmentName + "\nNumber of employees registered in department: " + this.employeeRegister.size()
                + "\nNumber of patients registered in department: " + this.patientRegister.size() + "\nTotal number of persons registered in department: "
                + (this.patientRegister.size() + this.employeeRegister.size());

    }

    /**
     * Equals method that checks for department name
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}
